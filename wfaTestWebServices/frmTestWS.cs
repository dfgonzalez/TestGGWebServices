﻿

//'{###################################################################################################################################################################################################
//'# Program Name : Test GRIN-Global Webservices
//'# Module Name : Test GRIN-Global Webservices
//'# Version : 0.1
//'# Purpose : Test GRIN-Global Webservices on accession passport/inventory management and list management, basic search and other functionalities.
//'# Dependencies: Visual Studio Express 2012, .NET Framework 4.0 and optionally connect to http://ggcommunity.cloudapp.net/gringlobal/gui.asmx or your local GG Webserver.
//'# Based on: Based on GRIN-Global source code 1.9.6.41 version and clone from https://jalarcon9198@bitbucket.org/jalarcon9198/gringlobal-client_v1.9.6.41_develop.git using Atlassian SourceTree.
//'# Reference: https://www.facebook.com/gringlobal/ and http://ggcommunity.cloudapp.net/gringlobal/ and http://www.ars-grin.gov/npgs/gringlobal/sb/home.html
//'# Data Started : 2016-06-01
//'# Date Last modification : 2016-03-22
//'# Copyright (c) 2016 by International Potato Center(CIP), Lima, Perú
//'# License: GPL (or other as indicated)
//'# Funding from: CIP and TRUST
//'# Contact: e.rojas@cgiar.org
//'# Author: Edwin Rojas (software developer - CIP)
//'# Author: Jose Guerrero (software developer - Consultant)
//'# Author: Juan Carlos Alarcon (software developer - CIMMYT)
//'#####################################################################################################################################################################################################

//Disclaimer
//The sample sorce code are not supported under any service or program. 
//The sample sorce code are provided AS IS without warranty of any kind. 
//CIP further disclaims all implied warranties including, without limitation, any implied warranties of merchantability or of fitness for a particular purpose. 
//The entire risk arising out of the use or performance of the sample sorce code and documentation remains with you. 



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace wfaTestWebServices
{
    public partial class frmTestWS : Form
    {
        public frmTestWS()
        {
            InitializeComponent();
        }
        public static string SHA1EncryptionBase64(string clearText)
        {
            string encryptedText = "";
            // Example encryption:  "administrator" = "s6ypLHk+4OmxqbCl9fwETgUUDfM="

            // First create a SHA1 class to do the encryption...
            System.Security.Cryptography.SHA1 sha1Hash = new System.Security.Cryptography.SHA1Managed();
            // This next line does three things:
            // 1) Encode the clearText into an array of UTF8 bytes 
            // 2) Pass the byte array to the SHA1 class (to compute the SHA1 hash)
            // 3) Convert the SHA1 encrypted bytes array to a Base 64 string (so that it can pass through to the webservice as standard text)...
            encryptedText = Convert.ToBase64String(sha1Hash.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(clearText)));

            return encryptedText;
        }
        private void butAddTab_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            if (String.IsNullOrEmpty(txtTabName.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            string tabname = txtTabName.Text;
            // 
            DataSet saveErrors = new DataSet();
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            // get user list items
            DataSet dsUserListItems = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_lists", ":cooperatorid=" + cooperatorID.ToString(), 0, 0, null);
            DataTable dtItemList = dsUserListItems.Tables["get_lists"].Copy();

            // add tab name
            DataRow newRecord;
            newRecord = dtItemList.NewRow();
            newRecord["app_user_item_list_id"] = -1;
            newRecord["COOPERATOR_ID"] = cooperatorID;
            newRecord["TAB_NAME"] = tabname;
            newRecord["LIST_NAME"] = "{DBNull.Value}";
            newRecord["ID_TYPE"] = "FOLDER";
            newRecord["SORT_ORDER"] = "0";
            newRecord["TITLE"] = tabname + " Root Folder";
            newRecord["PROPERTIES"] = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=;";
            newRecord["created_date"] = "2016-03-14T14:10:51.0548766";
            newRecord["created_by"] = cooperatorID;
            newRecord["owned_date"] = "2016-03-14T14:10:51.0548766";
            newRecord["owned_by"] = cooperatorID; ;
            // Now add it to the collection...
            dtItemList.Rows.Add(newRecord);
            // add folder
            newRecord = dtItemList.NewRow();
            newRecord["app_user_item_list_id"] = -2;
            newRecord["COOPERATOR_ID"] = cooperatorID;
            newRecord["TAB_NAME"] = tabname;
            newRecord["LIST_NAME"] = tabname + " Root Folder";
            newRecord["ID_TYPE"] = "FOLDER";
            newRecord["SORT_ORDER"] = 0;
            newRecord["TITLE"] = "New List";
            newRecord["PROPERTIES"] = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=;";
            newRecord["created_date"] = "2016-03-14T14:10:51.0548766";
            newRecord["created_by"] = cooperatorID;
            newRecord["owned_date"] = "2016-03-14T14:10:51.0548766";
            newRecord["owned_by"] = cooperatorID; ;
            // Now add it to the collection...
            dtItemList.Rows.Add(newRecord);
            // save changes
            DataSet dsChanges = new DataSet();
            dsChanges.Tables.Add(dtItemList.GetChanges());
            saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChanges, null);
            //////////////////////////////////////////////////////////////////
            // Get the user settings from the remote DB for the current user
            DataSet dsUserSettings = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_user_settings", ":cooperatorid=" + cooperatorID.ToString(), 0, 0, null);
            DataTable dtSettings = dsUserSettings.Tables["get_user_settings"].Copy();
            // get TabPages.Count aqui
            DataRow[] dr;
            dr = dtSettings.Select("cooperator_id='" + cooperatorID + "' AND resource_name='ux_tabcontrolGroupListNavigator' AND resource_key='TabPages.Count'");
            string TabPagesCount = dr[0]["resource_value"].ToString();
            // set TabPages.Count
            dr = dtSettings.Select("cooperator_id='" + cooperatorID + "' AND resource_name='ux_tabcontrolGroupListNavigator' AND resource_key='TabPages.Count'");
            dr[0]["resource_value"] = (Int32.Parse(TabPagesCount) + 1).ToString();
            // Create a new record...
            newRecord = dtSettings.NewRow();
            newRecord["app_user_gui_setting_id"] = -1;
            newRecord["cooperator_id"] = cooperatorID;
            newRecord["resource_name"] = tabname + "TreeView";
            newRecord["resource_key"] = "SelectedNode.FullPath";
            newRecord["resource_value"] = tabname + " Root Folder";
            dtSettings.Rows.Add(newRecord);
            // save TabPages.Order
            dr = dtSettings.Select("cooperator_id='" + cooperatorID + "' AND resource_name='ux_tabcontrolGroupListNavigator' AND resource_key='TabPages.Order'");
            string TabPagesOrder = dr[0]["resource_value"].ToString();
            TabPagesOrder += "|" + tabname;
            dr[0]["resource_value"] = TabPagesOrder;

            DataSet dsChangesSet = new DataSet();
            dsChangesSet.Tables.Add(dtSettings.GetChanges());

            saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChangesSet, null);
            saveErrors.WriteXml("d:\\borrar.txt");
            if (saveErrors.Tables.Contains("ExceptionTable"))
            {
                string cadena = "The tab was not added !!!";
                MessageBox.Show(cadena, "Error");
            }
            else
            {
                string mess = saveErrors.Tables["get_user_settings"].Rows[0].Field<string>("SavedStatus");
                if (mess == "Success")
                {
                    MessageBox.Show("The tab was added successfully !!!", "Message");
                }
            }
            

        }

        private void butSaveData_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            string prefix = txtPrefix.Text;
            int taxon = Int32.Parse(txtTaxon.Text);
            int number = Int32.Parse(txtNumber.Text);
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            DataSet dsAccession = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_accession", ":accessionid=; :taxonomyspeciesid=; :inventoryid=; :orderrequestid=; :geographyid=; :cooperatorid=; :taxonomygenusid=; ", 0, 10000, null);
            DataRow newRow = dsAccession.Tables["get_accession"].NewRow();

            newRow["accession_id"] = -1;
            newRow["accession_number_part1"] = prefix;
            newRow["accession_number_part2"] = number;
            newRow["taxonomy_species_id"] = taxon;
            newRow["status_code"] = "ACTIVE";
            newRow["is_core"] = "N";
            newRow["is_backed_up"] = "N";
            newRow["is_web_visible"] = "N";
            newRow["created_date"] = "2016-03-10T14:23:12.2925232";
            newRow["created_by"] = cooperatorID;
            newRow["owned_date"] = "2016-03-10T14:23:12.2995102";
            newRow["owned_by"] = cooperatorID;

            dsAccession.Tables["get_accession"].Rows.Add(newRow);
            DataSet saveErrors = new DataSet();
            saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsAccession, null);
            string mess = saveErrors.Tables["get_accession"].Rows[0].Field<string>("SavedStatus");
            if (mess == "Failure")
            {
                string cadena = saveErrors.Tables["get_accession"].Rows[0].Field<string>("ExceptionMessage");
                MessageBox.Show(cadena,"Error");
            }
            if (mess == "Success")
            {
                MessageBox.Show("The new accession was added successfully !!!","Message");
            }
            
        }

        private void butSaveInventory_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            string prefix = txtInvPrefix.Text;
            int invNumber = Int32.Parse(txtinvNumber.Text);
            string type = txtInvType.Text;
            int accessionId = Int32.Parse(txtAccessionId.Text);
            int policyId = Int32.Parse(txtPolicy.Text);
            
            // 
            DataSet saveErrors = new DataSet();
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            DataSet dsInventory = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_inventory", ":inventoryid=; :accessionid=; :inventorymaintpolicyid=; :orderrequestid=; :accessioninvgroupid=; ", 0, 10000, null);

            DataRow newRow = dsInventory.Tables["get_inventory"].NewRow();

            newRow["inventory_id"] = -1;
            newRow["inventory_number_part1"] = prefix;
            newRow["inventory_number_part2"] = invNumber; 
            newRow["form_type_code"] = type; 
            newRow["accession_id"] = accessionId;
            newRow["inventory_maint_policy_id"] = policyId; 
            newRow["site_id"] = -1;
            newRow["is_distributable"] = "N";
            newRow["is_auto_deducted"] = "N";
            newRow["is_available"] = "N";
            newRow["availability_status_code"] = "FLAW";  
            newRow["created_date"] = "2016-03-15T14:03:38.0740022";
            newRow["created_by"] = cooperatorID;
            newRow["owned_date"] = "2016-03-15T14:03:38.0879955";
            newRow["owned_by"] = cooperatorID;

            dsInventory.Tables["get_inventory"].Rows.Add(newRow);

            saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsInventory, null);
            string mess = saveErrors.Tables["get_inventory"].Rows[0].Field<string>("SavedStatus");
            if (mess == "Failure")
            {
                string cadena = saveErrors.Tables["get_inventory"].Rows[0].Field<string>("ExceptionMessage");
                MessageBox.Show(cadena, "Error");
            }
            if (mess == "Success")
            {
                MessageBox.Show("The new inventory was added successfully !!!", "Message");
            }
        }

        private void butChangeLanguage_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);
            int newLangID = this.cbLanguage.SelectedIndex + 1;

            // WebServices

            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            DataSet ds = new DataSet();
            ds = _GUIWebServices.ChangeLanguage(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), newLangID);
            MessageBox.Show("The Language was changed successfully !!!");
        }

        private void frmTestWS_Load(object sender, EventArgs e)
        {
            this.cbLanguage.SelectedIndex = 0;
            this.cbUsername.SelectedIndex = 1;
        }
        private void butValidateLogin_Click(object sender, EventArgs e)
        {
            // get data from form
            if (String.IsNullOrEmpty(txtUrl.Text.Trim()) || String.IsNullOrEmpty(cbUsername.Text.Trim()) || String.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            //int cooperatorID = Int32.Parse(txtUserId.Text);
            // WebServices

            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;
            DataSet dsUserData = new DataSet();
            // validate url
            try
            {
                dsUserData = _GUIWebServices.ValidateLogin(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            // messages
            if (dsUserData != null &&
                    dsUserData.Tables.Contains("validate_login") &&
                    dsUserData.Tables["validate_login"].Rows.Count > 0)
            {
                    int cooperatorId = dsUserData.Tables["validate_login"].Rows[0].Field<int>("cooperator_id");
                    txtUserId.Text = cooperatorId.ToString();
                    MessageBox.Show("The Username/Password are valid !!!");
                    tabControl1.Enabled = true;
            }
            else
            {
                MessageBox.Show("Error: The Username/Password are not valid for server:\n\n Please correct and try again.");
            }
        }
        private void butChangePassword_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);
            if (String.IsNullOrEmpty(txtNewPassword.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            string newPassword = txtNewPassword.Text;

            // WebServices

            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            DataSet ds = new DataSet();
            //ds = _GUIWebServices.ChangePassword(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), webServiceUsername, SHA1EncryptionBase64(newPassword));
            ds = _GUIWebServices.ChangePassword(true, webServiceUsername, webServicePassword, webServiceUsername, newPassword);

            if (ds != null && ds.Tables.Contains("ExceptionTable") && ds.Tables["ExceptionTable"].Rows.Count == 0)
            {
                MessageBox.Show("The password was changed successfully !!!");
            }
            else if (ds != null && ds.Tables.Contains("ExceptionTable") && ds.Tables["ExceptionTable"].Rows.Count > 0)
            {
                //MessageBox.Show("There was an unexpected error changing your password.\n\nFull error message:\n" + newPasswordResults.Tables["ExceptionTable"].Rows[0]["Message"].ToString());
                GGMessageBox ggMessageBox = new GGMessageBox("There was an unexpected error changing your password.\n\nFull error message:\n{0}", "Change Password Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ChangePassword_ux_buttonOKMessage1";
                if (ggMessageBox.MessageText.Contains("{0}")) ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, ds.Tables["ExceptionTable"].Rows[0]["Message"].ToString());
                ggMessageBox.ShowDialog();
            }
            else
            {
                //MessageBox.Show("There was an unexpected error changing your password.");
                GGMessageBox ggMessageBox = new GGMessageBox("There was an unexpected error changing your password.", "Change Password Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ChangePassword_ux_buttonOKMessage2";
                ggMessageBox.ShowDialog();
            }
        }

        private void butRenameTab_Click(object sender, EventArgs e)
        {
            // get data from form
            # region get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            if (String.IsNullOrEmpty(txtCurrTabName.Text.Trim()) || String.IsNullOrEmpty(txtNewTabName.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            string newtabname = txtNewTabName.Text;
            string currtabname = txtCurrTabName.Text;
            # endregion

            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            # region ACTUALIZACION TABLA lists
            DataSet ds = new DataSet();
            // el metodo RenameTab modifica solo la tabla lists, reemplaza el current tab name con el new tab name, para finalizar correctamente el
            // proceso se tiene que modificar la tabla user_settings usando un código adicionañl, revisar bloque CODIGO ADICIONAL
            ds = _GUIWebServices.RenameTab(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), currtabname, newtabname, cooperatorID);
            # endregion

            // bloque CODIGO ADICONAL
            # region ACTUALIZACION TABLA user_settings
            DataSet dsUserSettings = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_user_settings", ":cooperatorid=" + cooperatorID.ToString(), 0, 0, null);
            DataRow[] dr;
            dr = dsUserSettings.Tables["get_user_settings"].Select("cooperator_id='" + cooperatorID + "' AND resource_name='ux_tabcontrolGroupListNavigator' AND resource_key='TabPages.Order'");
            string TabPagesOrder = dr[0]["resource_value"].ToString();
            TabPagesOrder = TabPagesOrder.Replace(@currtabname, newtabname);
            dr[0]["resource_value"] = TabPagesOrder;
            DataSet dsChangesSet = new DataSet();
            dsChangesSet.Tables.Add(dsUserSettings.Tables["get_user_settings"].GetChanges());
            DataSet saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChangesSet, null);
            try
            {
                string mess = saveErrors.Tables["get_user_settings"].Rows[0].Field<string>("SavedStatus");
                if (mess == "Success")
                {
                    MessageBox.Show("The tab name was changed successfully !!!", "Message");
                }
            }
            catch
            {
                string cadena = "The tab name does not exist !!!";
                MessageBox.Show(cadena, "Error");
            }

            # endregion
        }

        private void butDeleteList_Click(object sender, EventArgs e)
        {
            // get data from form
            # region get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            string tabname = txtTabName2.Text;
            string searchstring = txtFullPath.Text;
            # endregion
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            // get_lists
            DataSet dsUserListItems = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_lists", ":cooperatorid=" + cooperatorID.ToString(), 0, 0, null);
            dsUserListItems.WriteXml("d:\\borrar.txt");
            string query = "list_name LIKE '%" + searchstring + "%' AND tab_name LIKE '%" + tabname + "%'";
            DataRow[] filteredRows = dsUserListItems.Tables["get_lists"].Select(query);
            if (filteredRows.Count() > 0)
            {
                foreach (DataRow row in filteredRows)
                {
                    row.Delete();
                }
                DataSet dsChangesSet = new DataSet();
                dsChangesSet.Tables.Add(dsUserListItems.Tables["get_lists"].GetChanges());
                DataSet saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChangesSet, null);
            }
            // separo el ultimo nodo de la cadena y defino la nueva cadena del campo list_name
            int position = searchstring.LastIndexOf('|');
            if (position > 0)
            {
                int lastPos = searchstring.Length - 1;
                string nodo = searchstring.Substring(position + 1, lastPos - position);
                string listname = searchstring.Remove(position);
                // elimino el registro que cumpla con la condicion
                query = "list_name LIKE '%" + listname + "%' AND tab_name LIKE '%" + tabname + "%' AND title LIKE '%" + nodo + "%'";
                filteredRows = dsUserListItems.Tables["get_lists"].Select(query);
                if (filteredRows.Count() > 0)
                {
                    foreach (DataRow row in filteredRows)
                    {
                        row.Delete();
                    }
                    DataSet dsChangesSet1 = new DataSet();
                    dsChangesSet1.Tables.Add(dsUserListItems.Tables["get_lists"].GetChanges());
                    DataSet saveErrors1 = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChangesSet1, null);
                    try
                    {
                        string mess = saveErrors1.Tables["get_lists"].Rows[0].Field<string>("SavedStatus");
                        if (mess == "Success")
                        {
                            MessageBox.Show("The list was removed successfully !!!", "Message");
                        }
                    }
                    catch
                    {
                        MessageBox.Show("There were problems removing list !!!", "Error");
                    }
                }
                else
                {
                    MessageBox.Show("Couldn´t delete the list !!!", "Error");
                }
            }
            else
            {
                MessageBox.Show("Couldn´t delete the list !!!", "Error");
            }
        }



        private void butAddList_Click(object sender, EventArgs e)
        {
            // get data from form
            # region get data from form
            if (String.IsNullOrEmpty(txtTabName3.Text.Trim()) || String.IsNullOrEmpty(txtFullPath1.Text.Trim()) || String.IsNullOrEmpty(txtNewNameList.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            string tabname = txtTabName3.Text;
            string fullpath = txtFullPath1.Text;
            string newlistname = txtNewNameList.Text;
            # endregion
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;
            // get user list items
            DataSet dsUserListItems = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_lists", ":cooperatorid=" + cooperatorID.ToString(), 0, 0, null);
            DataTable dtItemList = dsUserListItems.Tables["get_lists"].Copy();
            // check exist tab name
            int numtabname = dsUserListItems.Tables["get_lists"].Select("list_name = '{DBNull.Value}' and tab_name='"+tabname+"'").Length;
            if (numtabname < 1)
            {
                MessageBox.Show("The tab name does not exist !!!", "Message");
                return;
            }
            // check exist fullpath
            int position = fullpath.LastIndexOf('|');
            if (position < 0)
            {
                // check fullpath exists like root folder
                int numroot = dsUserListItems.Tables["get_lists"].Select("list_name = '{DBNull.Value}' and tab_name='" + tabname + "' and title='" + fullpath +"'").Length;
                if (numroot == 0)
                {
                    MessageBox.Show("The path does not exist !!!", "Message");
                    return;
                }
            }
            else
            {
                // check fullpath exist
                int lastPos = fullpath.Length - 1;
                string nodo = fullpath.Substring(position + 1, lastPos - position);
                string listname = fullpath.Remove(position);
                int numroot = dsUserListItems.Tables["get_lists"].Select("list_name = '" + listname + "' and tab_name='" + tabname + "' and title='" + nodo + "'").Length;
                if (numroot == 0)
                {
                    MessageBox.Show("The path does not exist !!!", "Message");
                    return;
                }
            }
            // add list
            // find value for field SORT_ORDER
            string query = "list_name = '" + fullpath + "'";
            int numberOfRecords = dsUserListItems.Tables["get_lists"].Select(query).Length;
            int sort_order = numberOfRecords + 1;
            // add new record (new list)
            DataRow newRecord;
            newRecord = dtItemList.NewRow();
            newRecord["app_user_item_list_id"] = -1;
            newRecord["COOPERATOR_ID"] = cooperatorID;
            newRecord["TAB_NAME"] = tabname;
            newRecord["LIST_NAME"] = fullpath;
            newRecord["ID_TYPE"] = "FOLDER";
            newRecord["SORT_ORDER"] = sort_order;
            newRecord["TITLE"] = newlistname;
            newRecord["PROPERTIES"] = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=;";
            newRecord["created_date"] = "2016-03-14T14:10:51.0548766";
            newRecord["created_by"] = cooperatorID;
            newRecord["owned_date"] = "2016-03-14T14:10:51.0548766";
            newRecord["owned_by"] = cooperatorID;

            // Now add it to the collection...
            dtItemList.Rows.Add(newRecord);
            // save changes
            DataSet dsChanges = new DataSet();
            dsChanges.Tables.Add(dtItemList.GetChanges());
            DataSet saveErrors = new DataSet();
            saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChanges, null);
            try
            {
                string mess = saveErrors.Tables["get_lists"].Rows[0].Field<string>("SavedStatus");
                if (mess == "Success")
                {
                    MessageBox.Show("The new list was created successfully !!!", "Message");
                }
            }
            catch
            {
                string cadena = "There were problems creating new list !!!";
                MessageBox.Show(cadena, "Error");
            }
        }
        private void butRenameListName_Click(object sender, EventArgs e)
        {
            // get data from form
            # region get data from form
            if (String.IsNullOrEmpty(txtTabName3.Text.Trim()) || String.IsNullOrEmpty(txtFullPath1.Text.Trim()) || String.IsNullOrEmpty(txtNewNameList.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            string tabname = txtTabName3.Text;
            string fullpath = txtFullPath1.Text;
            string newlistname = txtNewNameList.Text;
            # endregion
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            // get user list items
            DataSet dsUserListItems = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_lists", ":cooperatorid=" + cooperatorID.ToString(), 0, 0, null);
            // check exist tab name
            int numtabname = dsUserListItems.Tables["get_lists"].Select("list_name = '{DBNull.Value}' and tab_name='" + tabname + "'").Length;
            if (numtabname < 1)
            {
                MessageBox.Show("The tab name does not exist !!!", "Message");
                return;
            }
            // check exist fullpath
            int lastPos=0;
            string nodo="";
            string listname="";
            
            int position = fullpath.LastIndexOf('|');
            if (position < 0)
            {
                // check fullpath exists like root folder
                int numroot = dsUserListItems.Tables["get_lists"].Select("list_name = '{DBNull.Value}' and tab_name='" + tabname + "' and title='" + fullpath + "'").Length;
                if (numroot == 0)
                {
                    MessageBox.Show("The path does not exist !!!", "Message");
                    return;
                }
            }
            else
            {
                // check fullpath exist
                // separo el ultimo nodo de la cadena y defino el nuevo fullname con el nombre nuevo de la lista
                lastPos = fullpath.Length - 1;
                nodo = fullpath.Substring(position + 1, lastPos - position);
                listname = fullpath.Remove(position);
                int numroot = dsUserListItems.Tables["get_lists"].Select("list_name = '" + listname + "' and tab_name='" + tabname + "' and title='" + nodo + "'").Length;
                if (numroot == 0)
                {
                    MessageBox.Show("The path does not exist !!!", "Message");
                    return;
                }
            }            
            // rename list
            string query = "list_name LIKE '%" + fullpath + "%' AND tab_name LIKE '%" + tabname + "%'";
            DataRow[] filteredRows = dsUserListItems.Tables["get_lists"].Select(query);
            // new full name
            string newFN = listname + "|" + newlistname;
            // en los registros antes obtenidos, cambio el fullname por el nuevo fullname
            foreach (DataRow row in filteredRows)
            {
                string oldFullPath = row["list_name"].ToString();
                string newFullPath = oldFullPath.Replace(@fullpath, newFN);
                row["list_name"] = newFullPath;
            }
            // ultimo cambio
            query = "list_name = '" + listname + "' AND tab_name = '" + tabname + "' AND title = '" + nodo + "'";
            DataRow[] filteredRows2 = dsUserListItems.Tables["get_lists"].Select(query);
            foreach (DataRow row in filteredRows2)
            {
                row["title"] = newlistname;
            }
            // guardo los cambios
            DataSet dsChangesSet = new DataSet();
            dsChangesSet.Tables.Add(dsUserListItems.Tables["get_lists"].GetChanges());
            DataSet saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChangesSet, null);
            try
            {
                string mess = saveErrors.Tables["get_lists"].Rows[0].Field<string>("SavedStatus");
                if (mess == "Success")
                {
                    MessageBox.Show("The list was renamed successfully !!!", "Message");
                }
            }
            catch
            {
                string cadena = "There were problems renaming list !!!";
                MessageBox.Show(cadena, "Error");
            }

        }
        private void butAddItemToList_Click(object sender, EventArgs e)
        {
            // get data from form
            # region get data from form
            if (String.IsNullOrEmpty(txtTabName4.Text.Trim()) || String.IsNullOrEmpty(txtFullPath2.Text.Trim()) || String.IsNullOrEmpty(txtAccessionId2.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            string tabname = txtTabName4.Text;
            string fullpath = txtFullPath2.Text;
            int accessionId = Int32.Parse(txtAccessionId2.Text);
            # endregion
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;
            // get user list items
            DataSet dsUserListItems = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_lists", ":cooperatorid=" + cooperatorID.ToString(), 0, 0, null);
            DataTable dtItemList = dsUserListItems.Tables["get_lists"].Copy();
            // check exist tab name
            int numtabname = dsUserListItems.Tables["get_lists"].Select("list_name = '{DBNull.Value}' and tab_name='" + tabname + "'").Length;
            if (numtabname < 1)
            {
                MessageBox.Show("The tab name does not exist !!!", "Message");
                return;
            }
            // check exist fullpath
            int lastPos = 0;
            string nodo = "";
            string listname = "";

            int position = fullpath.LastIndexOf('|');
            if (position < 0)
            {
                // check fullpath exists like root folder
                int numroot = dsUserListItems.Tables["get_lists"].Select("list_name = '{DBNull.Value}' and tab_name='" + tabname + "' and title='" + fullpath + "'").Length;
                if (numroot == 0)
                {
                    MessageBox.Show("The path does not exist !!!", "Message");
                    return;
                }
            }
            else
            {
                // check fullpath exist
                // separo el ultimo nodo de la cadena y defino el nuevo fullname con el nombre nuevo de la lista
                lastPos = fullpath.Length - 1;
                nodo = fullpath.Substring(position + 1, lastPos - position);
                listname = fullpath.Remove(position);
                int numroot = dsUserListItems.Tables["get_lists"].Select("list_name = '" + listname + "' and tab_name='" + tabname + "' and title='" + nodo + "'").Length;
                if (numroot == 0)
                {
                    MessageBox.Show("The path does not exist !!!", "Message");
                    return;
                }
            }     

            // find value for field SORT_ORDER
            string query = "list_name = '" + fullpath + "'";
            int sort_order = dsUserListItems.Tables["get_lists"].Select(query).Length;
            // find value for field TITLE
            DataSet dsAccession = new DataSet();
            dsAccession = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_accession", ":accessionid=" + accessionId.ToString() + "; :taxonomyspeciesid=; :inventoryid=; :orderrequestid=; :geographyid=; :cooperatorid=; :taxonomygenusid=; ", 0, 10000, null);
            if (dsAccession.Tables[1].Rows.Count == 0)
            {
                MessageBox.Show("The accession id does not exist !!!", "Message");
                return;
            }

            string prefix = dsAccession.Tables["get_accession"].Rows[0].Field<string>("accession_number_part1");
            int an = dsAccession.Tables["get_accession"].Rows[0].Field<int>("accession_number_part2");
            string title = prefix + "_" + an.ToString();
            // add new record (new list)
            DataRow newRecord;
            newRecord = dtItemList.NewRow();
            newRecord["app_user_item_list_id"] = -1;
            newRecord["COOPERATOR_ID"] = cooperatorID;
            newRecord["TAB_NAME"] = tabname;
            newRecord["LIST_NAME"] = fullpath;
            newRecord["ID_NUMBER"] = accessionId;
            newRecord["ID_TYPE"] = "ACCESSION_ID";
            newRecord["SORT_ORDER"] = sort_order;
            newRecord["TITLE"] = title;
            newRecord["PROPERTIES"] = "ACCESSION_ID;:accessionid=" + accessionId.ToString() + ";@accession.accession_id=" + accessionId.ToString();
            newRecord["created_date"] = "2016-03-14T14:10:51.0548766";
            newRecord["created_by"] = cooperatorID;
            newRecord["owned_date"] = "2016-03-14T14:10:51.0548766";
            newRecord["owned_by"] = cooperatorID; ;
            // Now add it to the collection...
            dtItemList.Rows.Add(newRecord);
            // save changes
            DataSet dsChanges = new DataSet();
            dsChanges.Tables.Add(dtItemList.GetChanges());
            DataSet saveErrors = new DataSet();
            saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChanges, null);
            string mess = saveErrors.Tables["get_lists"].Rows[0].Field<string>("SavedStatus");
            if (mess == "Failure")
            {
                string cadena = saveErrors.Tables["get_lists"].Rows[0].Field<string>("ExceptionMessage");
                MessageBox.Show(cadena, "Error");
            }
            if (mess == "Success")
            {
                MessageBox.Show("The new item was added successfully !!!", "Message");
            }
        }


        private void butDeleteItem_Click(object sender, EventArgs e)
        {
            // get data from form
            # region get data from form
            if (String.IsNullOrEmpty(txtTabName5.Text.Trim()) || String.IsNullOrEmpty(txtFullPath3.Text.Trim()) || String.IsNullOrEmpty(txtItemName.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            string tabname = txtTabName5.Text;
            string fullpath = txtFullPath3.Text;
            string itemname = txtItemName.Text;
            # endregion
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            // get_lists
            DataSet dsUserListItems = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_lists", ":cooperatorid=" + cooperatorID.ToString(), 0, 0, null);
            // check exist tab name
            int numtabname = dsUserListItems.Tables["get_lists"].Select("list_name = '{DBNull.Value}' and tab_name='" + tabname + "'").Length;
            if (numtabname < 1)
            {
                MessageBox.Show("The tab name does not exist !!!", "Message");
                return;
            }
            // check exist fullpath
            int lastPos = 0;
            string nodo = "";
            string listname = "";

            int position = fullpath.LastIndexOf('|');
            if (position < 0)
            {
                // check fullpath exists like root folder
                int numroot = dsUserListItems.Tables["get_lists"].Select("list_name = '{DBNull.Value}' and tab_name='" + tabname + "' and title='" + fullpath + "'").Length;
                if (numroot == 0)
                {
                    MessageBox.Show("The path does not exist !!!", "Message");
                    return;
                }
            }
            else
            {
                // check fullpath exist
                // separo el ultimo nodo de la cadena y defino el nuevo fullname con el nombre nuevo de la lista
                lastPos = fullpath.Length - 1;
                nodo = fullpath.Substring(position + 1, lastPos - position);
                listname = fullpath.Remove(position);
                int numroot = dsUserListItems.Tables["get_lists"].Select("list_name = '" + listname + "' and tab_name='" + tabname + "' and title='" + nodo + "'").Length;
                if (numroot == 0)
                {
                    MessageBox.Show("The path does not exist !!!", "Message");
                    return;
                }
            }     
            // delete
            string query = "list_name ='" + fullpath + "' AND tab_name ='" + tabname + "' AND title ='" + itemname + "'";
            DataRow[] filteredRows = dsUserListItems.Tables["get_lists"].Select(query);
            if (filteredRows.Count() > 0)
            {
                foreach (DataRow row in filteredRows)
                {
                    //Console.Write(row[0].ToString() + " ");
                    row.Delete();
                }
                DataSet dsChangesSet = new DataSet();
                dsChangesSet.Tables.Add(dsUserListItems.Tables["get_lists"].GetChanges());
                DataSet saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChangesSet, null);
                string mess = saveErrors.Tables["get_lists"].Rows[0].Field<string>("SavedStatus");
                if (mess == "Failure")
                {
                    string cadena = saveErrors.Tables["get_lists"].Rows[0].Field<string>("ExceptionMessage");
                    MessageBox.Show(cadena, "Error");
                }
                if (mess == "Success")
                {
                    MessageBox.Show("The item was deleted successfully !!!", "Message");
                }
            }
            else
            {
                MessageBox.Show("The item does not exist !!!", "Message");
            }
        }

        private void butSearchByNumber_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            int an = Int32.Parse(txtSearchAccession1.Text);
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            
            string query="@accession.accession_number_part2 = "+an;
            bool ignoreCase=true;
            bool andTermsTogether=true;
            string indexList="accession accession_action accession_inv_annotation accession_ipr accession_pedigree accession_quarantine accession_source accession_source_map accession_inv_voucher citation cooperator cooperator_group cooperator_map crop crop_trait crop_trait_code crop_trait_code_lang crop_trait_observation geography inventory inventory_action accession_inv_group accession_inv_group_map inventory_maint_policy inventory_quality_status inventory_viability literature method method_map order_request order_request_action order_request_item site taxonomy_author taxonomy_common_name taxonomy_family taxonomy_genus taxonomy_use accession_inv_attach accession_inv_name ";
            string resolverName="accession";
            int offset=0;
            int limit=15000;
            string searchOptions="SearchEngineType='dynamic'";

            DataSet dsIdAccession = _GUIWebServices.Search(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, searchOptions);
            if (dsIdAccession.Tables["SearchResult"].Rows.Count != 0)
            {
                string result="";
                foreach (DataRow row in dsIdAccession.Tables["SearchResult"].Rows)
                {
                    int id = Int32.Parse(row[0].ToString());
                    DataSet dsAcc = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_accession", ":accessionid=" + id.ToString() + "; :taxonomyspeciesid=; :inventoryid=; :orderrequestid=; :geographyid=; :cooperatorid=; :taxonomygenusid=; ", 0, 15000, null);
                    StringWriter sw = new StringWriter();
                    dsAcc.WriteXml(sw);
                    result = result + sw.ToString();
                }
                GGMessageBox ggMessageBox = new GGMessageBox(result, "", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "GrinGlobalClient_LoadMessage2";
                ggMessageBox.ShowDialog();
            }
            else{
                MessageBox.Show("There is not record to show !!!");
            }
        }

        private void butSearchByName_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            string name = txtSearchAccession2.Text;
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;


            string query = "@accession_inv_name.plant_name = '" + name+"'";
            bool ignoreCase = true;
            bool andTermsTogether = true;
            string indexList = "accession accession_action accession_inv_annotation accession_ipr accession_pedigree accession_quarantine accession_source accession_source_map accession_inv_voucher citation cooperator cooperator_group cooperator_map crop crop_trait crop_trait_code crop_trait_code_lang crop_trait_observation geography inventory inventory_action accession_inv_group accession_inv_group_map inventory_maint_policy inventory_quality_status inventory_viability literature method method_map order_request order_request_action order_request_item site taxonomy_author taxonomy_common_name taxonomy_family taxonomy_genus taxonomy_use accession_inv_attach accession_inv_name ";
            string resolverName = "accession";
            int offset = 0;
            int limit = 15000;
            string searchOptions = "SearchEngineType='dynamic'";

            DataSet dsIdAccession = _GUIWebServices.Search(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, searchOptions);
            if (dsIdAccession.Tables["SearchResult"].Rows.Count != 0)
            {
                string result = "";
                foreach (DataRow row in dsIdAccession.Tables["SearchResult"].Rows)
                {
                    int id = Int32.Parse(row[0].ToString());
                    DataSet dsAcc = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_accession", ":accessionid=" + id.ToString() + "; :taxonomyspeciesid=; :inventoryid=; :orderrequestid=; :geographyid=; :cooperatorid=; :taxonomygenusid=; ", 0, 15000, null);
                    StringWriter sw = new StringWriter();
                    dsAcc.WriteXml(sw);
                    result = result + sw.ToString();
                }
                GGMessageBox ggMessageBox = new GGMessageBox(result, "", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "GrinGlobalClient_LoadMessage2";
                ggMessageBox.ShowDialog();
            }
            else
            {
                MessageBox.Show("There is not record to show !!!");
            }
        }


        private void butSearchByTaxonomy_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            int idTaxon = Int32.Parse(txtSearchAccession3.Text);
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;
            
            string query = "@accession.taxonomy_species_id IN (" + idTaxon + ")";
            bool ignoreCase = true;
            bool andTermsTogether = true;
            string indexList = "accession accession_action accession_inv_annotation accession_ipr accession_pedigree accession_quarantine accession_source accession_source_map accession_inv_voucher citation cooperator cooperator_group cooperator_map crop crop_trait crop_trait_code crop_trait_code_lang crop_trait_observation geography inventory inventory_action accession_inv_group accession_inv_group_map inventory_maint_policy inventory_quality_status inventory_viability literature method method_map order_request order_request_action order_request_item site taxonomy_author taxonomy_common_name taxonomy_family taxonomy_genus taxonomy_use accession_inv_attach accession_inv_name ";
            string resolverName = "accession";
            int offset = 0;
            int limit = 15000;
            string searchOptions = "SearchEngineType='dynamic'";

            DataSet dsIdAccession = _GUIWebServices.Search(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, searchOptions);
            if (dsIdAccession.Tables["SearchResult"].Rows.Count != 0)
            {
                string result = "";
                foreach (DataRow row in dsIdAccession.Tables["SearchResult"].Rows)
                {
                    int id = Int32.Parse(row[0].ToString());
                    DataSet dsAcc = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_accession", ":accessionid=" + id.ToString() + "; :taxonomyspeciesid=; :inventoryid=; :orderrequestid=; :geographyid=; :cooperatorid=; :taxonomygenusid=; ", 0, 15000, null);
                    StringWriter sw = new StringWriter();
                    dsAcc.WriteXml(sw);
                    result = result + sw.ToString();
                }
                GGMessageBox ggMessageBox = new GGMessageBox(result, "", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                //ggMessageBox.Name = "";
                ggMessageBox.ShowDialog();
            }
            else
            {
                MessageBox.Show("There is not record to show !!!");
            }
        }

        private void butSearchInventory_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            int an = Int32.Parse(txtSearchInventory.Text);
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            string query = "@inventory.accession_id IN (" + an + ")";
            bool ignoreCase = true;
            bool andTermsTogether = true;
            string indexList = "accession accession_action accession_inv_annotation accession_ipr accession_pedigree accession_quarantine accession_source accession_source_map accession_inv_voucher citation cooperator cooperator_group cooperator_map crop crop_trait crop_trait_code crop_trait_code_lang crop_trait_observation geography inventory inventory_action accession_inv_group accession_inv_group_map inventory_maint_policy inventory_quality_status inventory_viability literature method method_map order_request order_request_action order_request_item site taxonomy_author taxonomy_common_name taxonomy_family taxonomy_genus taxonomy_use accession_inv_attach accession_inv_name ";
            string resolverName = "inventory";
            int offset = 0;
            int limit = 1000;
            string searchOptions = "SearchEngineType='dynamic'";

            DataSet dsIdAccession = _GUIWebServices.Search(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, searchOptions);

            if (dsIdAccession.Tables["SearchResult"].Rows.Count != 0)
            {
                string result = "";
                foreach (DataRow row in dsIdAccession.Tables["SearchResult"].Rows)
                {
                    int id = Int32.Parse(row[0].ToString());
                    DataSet dsAcc = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_inventory",":inventoryid="+id.ToString()+"; :accessionid=; :inventorymaintpolicyid=; :orderrequestid=; :accessioninvgroupid=; ", 0, 1000, null);
                    StringWriter sw = new StringWriter();
                    dsAcc.WriteXml(sw);
                    result = result + sw.ToString();
                }
                GGMessageBox ggMessageBox = new GGMessageBox(result, "", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.ShowDialog();
            }
            else
            {
                MessageBox.Show("There is not record to show !!!");
            }
        }

        private void butChangeAccNumber_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            if (String.IsNullOrEmpty(txtAccessionId3.Text.Trim()) || String.IsNullOrEmpty(txtNewNumber.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            int accessionId = Int32.Parse(txtAccessionId3.Text);
            int accessionNumber = Int32.Parse(txtNewNumber.Text);
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            // get accession using id
            DataSet dsAcc = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_accession", ":accessionid=" + accessionId.ToString() + "; :taxonomyspeciesid=; :inventoryid=; :orderrequestid=; :geographyid=; :cooperatorid=; :taxonomygenusid=; ", 0, 15000, null);
            if (dsAcc.Tables["get_accession"].Rows.Count == 0)
            {
                MessageBox.Show("The accession id does not exist !!!");
                return;
            }
            // change name
            dsAcc.Tables["get_accession"].Rows[0].SetField("accession_number_part2", accessionNumber);
            DataSet dsChangesSet = new DataSet();
            dsChangesSet.Tables.Add(dsAcc.Tables["get_accession"].GetChanges());

            DataSet saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChangesSet, null);
            try
            {
                string mess = saveErrors.Tables["get_accession"].Rows[0].Field<string>("SavedStatus");
                if (mess == "Success")
                {
                    MessageBox.Show("The accession number was changed successfully !!!", "Message");
                }
            }
            catch
            {
                string cadena = "There were problems changing accession number !!!";
                MessageBox.Show(cadena, "Error");
            }
            
        }

        private void butChangeQoh_Click(object sender, EventArgs e)
        {
            // get data from form
            string webServiceURL = txtUrl.Text;
            string webServiceUsername = cbUsername.Text;
            string webServicePassword = txtPassword.Text;
            int cooperatorID = Int32.Parse(txtUserId.Text);

            if (String.IsNullOrEmpty(txtInventoryId.Text.Trim()) || String.IsNullOrEmpty(txtQoh.Text.Trim()))
            {
                MessageBox.Show("Enter a valid value !!!");
                return;
            }
            int inventoryId = Int32.Parse(txtInventoryId.Text);
            int qoh = Int32.Parse(txtQoh.Text);
            string qohUnit = txtQohUnit.Text;
            // WebServices
            GrinGlobalGUIWebServices.GUI _GUIWebServices = new GrinGlobalGUIWebServices.GUI();
            _GUIWebServices.Url = webServiceURL;

            // get accession using id
            DataSet dsAcc = _GUIWebServices.GetData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), "get_inventory", ":inventoryid=" + inventoryId.ToString() + "; :accessionid=; :inventorymaintpolicyid=; :orderrequestid=; :accessioninvgroupid=; ", 0, 15000,null);
            if (dsAcc.Tables["get_inventory"].Rows.Count == 0)
            {
                MessageBox.Show("The inventory id does not exist !!!");
                return;
            }
            // change name
            dsAcc.Tables["get_inventory"].Rows[0].SetField("quantity_on_hand", qoh);
            dsAcc.Tables["get_inventory"].Rows[0].SetField("quantity_on_hand_unit_code", qohUnit);

            DataSet dsChangesSet = new DataSet();
            dsChangesSet.Tables.Add(dsAcc.Tables["get_inventory"].GetChanges());

            DataSet saveErrors = _GUIWebServices.SaveData(true, webServiceUsername, SHA1EncryptionBase64(webServicePassword), dsChangesSet, null);

            try
            {
                string mess = saveErrors.Tables["get_inventory"].Rows[0].Field<string>("SavedStatus");
                if (mess == "Success")
                {
                    MessageBox.Show("The quantity on hand was changed successfully !!!", "Message");
                }
                if (mess == "Failure")
                {
                    string cadena = saveErrors.Tables["get_inventory"].Rows[0].Field<string>("ExceptionMessage");
                    MessageBox.Show(cadena, "Error");
                }
            }
            catch
            {
                string cadena = "There were problems changing quantity on hand !!!";
                MessageBox.Show(cadena, "Error");
            }
        }




    }
}
